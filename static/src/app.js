import React from 'react'
import ReactDOM from 'react-dom'
import App from './views/index'

let root = document.getElementById('app')

ReactDOM.render(<App />, root)

