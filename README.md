# webpack scripts

 一个较为常用的 `webpack + babel` 的配置


### 默认的文件夹结构

```
- static
    - dist - 编译的目标文件夹
    - src - 编译的源文件夹

- build - 编译脚本都放在这里
- package.json - 项目介绍、依赖和脚本入口
```

### 三个 npm script

- npm run build: 编译最终目标文件（会压缩 js 和 css，编译最慢）
- npm run dev: 开发编译，会 watch 当前文件夹的变化，每次变化都会重新编译
- npm run serve: 本地开发常用，会在本地的 8080 端口启动一个 server。同样会 watch 当前文件夹的变化并编译，并且会在 8080
  端口的页面中，自动更新配置中的 output 文件。
