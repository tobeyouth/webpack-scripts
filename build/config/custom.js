/**
 * custom config
 */

const constants = require('./const')

module.exports = {
  entry: {
    app: constants.appPath
  },
  devServer: {
    host: 'localhost',
    port: '8000',
    open: true,
    proxy: {
      '/api': 'http://dae-pre52.dapps.douban.com'
    }
  }
}
